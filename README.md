# About Code Review
## Why Code Review?
1. The costs to remedy a defect are up to 10 to 100 times less in the early operations compared to fixing a defect in the maintenance phase([citation here](https://en.wikipedia.org/wiki/Fagan_inspection#cite_ref-Fagan_1976_182%E2%80%93211_1-0)), it is essential to find defects as close to the point of insertion as possible. 
2. Average project spends 50-80% of its total effort correcting mistakes. How is that possible? [Most projects wait until the red zone to correct their defects. Better run projects find most defects in the blue zone.](https://www.construx.com/wp-content/uploads/resources/posters/Defect%20Cost%20Increase%20Poster%20by%20Construx.pdf)

## How code review works ?
This is going to take some time. And is important. Please set apart some 1-2 hours to read this thoroughly. Lots of links, lots of articles to read.

Drawing inspiration from [Google's Code Review Developer Guide](https://google.github.io/eng-practices/review/) you either `AUTHOR` a pull request (a.k.a [CL Author](https://github.com/google/eng-practices#terminology)), Or you `REVIEW` a pull request. Depending on whether you are `AUTHOR` or `REVIEWER` you would need to through in detail the following two guides. 
- [How To Do A Code Review](https://google.github.io/eng-practices/review/reviewer/): A detailed guide for code reviewers.
- [The CL Author’s Guide](https://google.github.io/eng-practices/review/developer/): A detailed guide for developers whose CLs are going through review.

In this project we are compiling code review best practices by scouting them from around the world. These best practices are organized under following [labels](https://docs.gitlab.com/ee/user/project/labels.html).  
- #1 ~Review-Design: Is the code well-designed and appropriate for your system?
- #2 ~Review-Functionality: Does the code behave as the author likely intended? Is the way the code behaves good for its users? 
- #3 ~Review-Complexity: Could the code be made simpler? Would another developer be able to easily use this code when they come across it in the future? 
- #4 ~Review-Tests: Does the code have correct and well-designed automated tests? 
- #5 ~Review-Naming: Did the developer choose clear names for variables, classes, methods, etc.? 
- #6 ~Review-Comments: Are the comments clear and useful? 
- #7 ~Review-Style: Does the code follow our style guides? 
- #8 ~Review-Documentation: Did the developer also update relevant documentation? 

## How to review code in Smarter.Codes?
Refer to [7 step process in Coding Guidelines](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/about-coding)
* And then [Use CONTRIBUTING.md file to define the guidelines your project must be reviewed upon](#12)
* Send it for review
* Reviewer would also refer or contribute to CONTRIBUTING.md file to suggest what guidelines we must abide to in the code review
* Reviewers are expected to [take enough time to review](https://smartbear.com/learn/code-review/best-practices-for-peer-code-review/) <sup>Read point 1,2, and 3</sup>. And not merge any request until the work complies with all guidelines mentioned in CONTRIBUTING.md file. If the work was not complying with a certain few guidelines, we expect reviewer to mention URLs of the guidelines which were left out.
* Discuss with colleagues if some guideline must be violated. Consider removing it from CONTRIBUTING.md file then

# Contribute to Review Guidelines
1. Do [Formal Inspection, or  Regular change-based code review](https://en.wikipedia.org/wiki/Code_review#Types_of_review_processes) of your project. Take note of these suggestions
2. For each suggestion, post a new issue in [Software Engineering Guidelines at Smarter.Codes](../../../../). Label issue as `~Review-Design`, `~Review-Functionality`, etc
3. Back your suggestion with citations. Find an article on internet where programming community is giving the same suggestion.
4. [Link to this guideline issue from in your project](https://smartercodes.sharepoint.com/sites/AppApprovalPortal/Shared%20Documents/General/Recordings/MRs%20linked%20to%20Guidelines-20210224_092457-Meeting%20Recording.mp4?web=1). Either create a new issue in your project, or paste a link to this guideline from inside the Merge Request you were reviewing.

For each guideline issue kindly set their [priority label](#10) as follows
- ~"Priority::A (Essential)". These rules help prevent errors, so learn and abide by them at all costs.
- ~"Priority::B (Strongly Recommended)". Your code will still run if you violate them, but violations should be rare and well-justified.
- ~"Priority::C (Recommended)". Where multiple, equally good options exist, an arbitrary choice can be made to ensure consistency.
- ~"Priority::D (Use with Caution)". They exist to accommodate rare edge cases. When overused however, they can make your code more difficult to maintain

Ask your peers engineers in Smarter.Codes to Upvote or downvote issues. Take sides on which code review practice you like or dislike/ Post comments inside issues. Agree or disagree with a best practice. In your comments link to an external article showing 'good or  bad example' of how to write code.


# Inspirations
* [Google's Code Review Developer Guide](https://google.github.io/eng-practices/review/)
* [SmartBear's Best Practices for Code Review](https://smartbear.com/learn/code-review/best-practices-for-peer-code-review/)

## See Also
[Automated Code Review](../../../../automated-code-review)
